package com.ordinarynick.jmeter.service.utils;

import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.util.Optional;

import static java.time.temporal.ChronoField.DAY_OF_MONTH;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static java.time.temporal.ChronoField.MONTH_OF_YEAR;
import static java.time.temporal.ChronoField.SECOND_OF_MINUTE;
import static java.time.temporal.ChronoField.YEAR;

/**
 * Utility class for date methods.
 */
@UtilityClass
public class DateUtils {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2)
            .optionalStart()
            .appendLiteral(':')
            .appendValue(SECOND_OF_MINUTE, 2)
            .appendLiteral(" ")
            .appendValue(DAY_OF_MONTH, 2)
            .appendLiteral('-')
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral('-')
            .appendValue(YEAR, 4, 10, SignStyle.EXCEEDS_PAD)
            .parseLenient()
            .parseStrict()
            .toFormatter();

    /**
     * Converts {@link OffsetDateTime} to formatted {@link String}.
     *
     * @param convert {@link OffsetDateTime} to be converted.
     * @return {@link String} with date and time.
     */
    public static String toString(final OffsetDateTime convert) {
        return Optional.ofNullable(convert)
                .map(offsetDateTime -> offsetDateTime.atZoneSameInstant(getSystemOffset()))
                .map(dateTime -> dateTime.format(DATE_TIME_FORMATTER))
                .orElse("");
    }

    /**
     * Parse {@link String} to {@link OffsetDateTime}.
     *
     * @param parse {@link String} to parsed.
     * @return {@link OffsetDateTime} instance or null if input is null.
     * @throws java.time.format.DateTimeParseException If {@link String} could not be parsed.
     */
    public OffsetDateTime toEntity(final String parse) {
        return Optional.ofNullable(parse)
                .map(dateTime -> OffsetDateTime.of(LocalDateTime.parse(parse, DATE_TIME_FORMATTER), getSystemOffset()))
                .orElse(null);
    }

    private ZoneOffset getSystemOffset() {
        return OffsetDateTime.now().getOffset();
    }
}
