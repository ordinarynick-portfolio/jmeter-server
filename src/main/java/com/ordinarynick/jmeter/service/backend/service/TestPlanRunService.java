package com.ordinarynick.jmeter.service.backend.service;

import com.ordinarynick.jmeter.service.backend.entity.TestPlan;
import com.ordinarynick.jmeter.service.backend.entity.TestPlanRun;
import com.ordinarynick.jmeter.service.backend.entity.TestPlanRunState;
import com.ordinarynick.jmeter.service.backend.repository.TestPlanRunRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for {@link TestPlan} entity.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Slf4j
public class TestPlanRunService extends CrudService<TestPlanRun> {

    @Autowired
    public TestPlanRunService(final TestPlanRunRepository repository) {
        super(repository);
    }

    /**
     * Run (schedule) {@link TestPlanRun} to be run on runner.
     *
     * @param testPlan       {@link TestPlan} to be run.
     * @param environmentUrl Against this environment will be {@link TestPlan} run.
     */
    public void run(final TestPlan testPlan, final String environmentUrl) {
        final var run = TestPlanRun.builder()
                .testPlan(testPlan)
                .environmentUrl(environmentUrl)
                .state(TestPlanRunState.PENDING)
                .build();

        save(run);
        // TODO and schedule test plan run to be run.
    }
}
