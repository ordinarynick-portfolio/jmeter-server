package com.ordinarynick.jmeter.service.backend.repository;

import com.ordinarynick.jmeter.service.backend.entity.TestPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Database {@link JpaRepository} for {@link TestPlan} entity.
 */
@Repository
public interface TestPlanRepository extends CommonJpaRepository<TestPlan> {

}
