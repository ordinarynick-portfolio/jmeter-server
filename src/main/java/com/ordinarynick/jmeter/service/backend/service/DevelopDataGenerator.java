package com.ordinarynick.jmeter.service.backend.service;

import com.ordinarynick.jmeter.service.backend.entity.TestPlan;
import com.ordinarynick.jmeter.service.backend.entity.TestPlanRun;
import com.ordinarynick.jmeter.service.backend.entity.TestPlanRunLogRow;
import com.ordinarynick.jmeter.service.backend.entity.TestPlanRunState;
import com.ordinarynick.jmeter.service.backend.repository.TestPlanRepository;
import com.ordinarynick.jmeter.service.backend.repository.TestPlanRunRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Random;

@Service
@AllArgsConstructor(onConstructor = @__({@Autowired}))
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DevelopDataGenerator {

    private static final String[] words = new String[]{"nothing", "strange", "anything", "is", "something", "nick",
            "webpage", "game", "hello", "world", "canary", "earth", "space"};

    TestPlanRepository testPlanRepository;
    TestPlanRunRepository testPlanRunRepository;

    Random random = new Random(123);

    @PostConstruct
    public void generateData() {
        for (var i = random.nextInt(25); i >= 0; i--) {
            createTestPlan();
        }
    }

    private void createTestPlan() {
        final var testPlan = testPlanRepository.save(generateTestPlan());

        for (var i = random.nextInt(10); i >= 0; i--) {
            final var run = testPlanRunRepository.save(generateRun(testPlan));
            for (var j = random.nextInt(5); j >= 0; j--) {
                run.getLogEntries().add(generateLogRow());
            }
            testPlanRunRepository.save(run);
        }
    }

    private TestPlan generateTestPlan() {
        return TestPlan.builder()
                .name(words[random.nextInt(words.length)])
                .file("file://" + words[random.nextInt(words.length)] + ".jmx")
                .build();
    }

    private TestPlanRun generateRun(final TestPlan testPlan) {
        return TestPlanRun.builder()
                .environmentUrl("https://" + words[random.nextInt(words.length)] + ".com")
                .state(TestPlanRunState.values()[random.nextInt(TestPlanRunState.values().length)])
                .testPlan(testPlan)
                .build();
    }

    private TestPlanRunLogRow generateLogRow() {
        return TestPlanRunLogRow.builder()
                .data("Some log row text.")
                .build();
    }
}
