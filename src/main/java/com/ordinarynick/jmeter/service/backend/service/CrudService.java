package com.ordinarynick.jmeter.service.backend.service;

import com.ordinarynick.jmeter.service.backend.entity.AbstractEntity;
import com.ordinarynick.jmeter.service.backend.repository.CommonJpaRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Common service definition with implementation of common methods.
 *
 * @param <T> Type of entity.
 */
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Slf4j
public abstract class CrudService<T extends AbstractEntity> {

    CommonJpaRepository<T> repository;

    /**
     * Find all {@link T}s in database.
     *
     * @param search {@link String} for full text search.
     * @return {@link List} of all found {@link T}s.
     */
    @Transactional(readOnly = true)
    public List<T> findAll(final String search) {
        return Optional.ofNullable(search)
                .filter(StringUtils::isNotBlank)
                .map(repository::search)
                .orElseGet(repository::findAll);
    }

    /**
     * Saves entity into DB.
     *
     * @param entity {@link T} entity which will be saved.
     * @return Saved {@link T} instance.
     */
    @Transactional
    public T save(final T entity) {
        return repository.save(entity);
    }
}
