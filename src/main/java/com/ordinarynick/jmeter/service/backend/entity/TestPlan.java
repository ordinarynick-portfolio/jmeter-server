package com.ordinarynick.jmeter.service.backend.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Test plan entity, which can be run. Steps of test plan is defined by file.
 */
@Entity
@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@EntityListeners(AuditingEntityListener.class)
public class TestPlan extends AbstractAuditedEntity implements Serializable {

    /**
     * Name of test plan for user.
     */
    @NotNull
    @NotEmpty
    String name;

    /**
     * Path to Test plan file.
     */
    @NotNull
    @NotEmpty
    String file;

    /**
     * Log row for this run.
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<TestPlanRun> runs = new ArrayList<>();

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final var testPlan = (TestPlan) o;
        return Objects.equals(getId(), testPlan.getId())
                && Objects.equals(name, testPlan.name)
                && Objects.equals(file, testPlan.file);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, file);
    }
}
