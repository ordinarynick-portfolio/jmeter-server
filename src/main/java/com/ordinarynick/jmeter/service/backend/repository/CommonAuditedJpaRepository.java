package com.ordinarynick.jmeter.service.backend.repository;

import com.ordinarynick.jmeter.service.backend.entity.AbstractAuditedEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Common repository for {@link AbstractAuditedEntity} with some common methods.
 *
 * @param <T> Child class of {@link AbstractAuditedEntity}.
 */
@NoRepositoryBean
public interface CommonAuditedJpaRepository<T extends AbstractAuditedEntity> extends CommonJpaRepository<T> {

    /**
     * Search for entities via full text search.
     *
     * @param search {@link String} as search text.
     * @return {@link List} with found entities.
     */
    @Query("select t "
            + "from #{#entityName} t "
            + "where "
            + "trim(t.id) like lower(concat('%', :searchTerm, '%')) "
            + "or lower(t.created) like lower(concat('%', :searchTerm, '%')) "
            + "or lower(t.lastChange) like lower(concat('%', :searchTerm, '%'))"
    )
    List<T> search(@Param("searchTerm") final String search);
}
