package com.ordinarynick.jmeter.service.backend.repository;

import com.ordinarynick.jmeter.service.backend.entity.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Common repository for {@link AbstractEntity} with some common methods.
 *
 * @param <T> Child class of {@link AbstractEntity}.
 */
@NoRepositoryBean
public interface CommonJpaRepository<T extends AbstractEntity> extends JpaRepository<T, Long> {

    /**
     * Search for entities via full text search.
     *
     * @param search {@link String} as search text.
     * @return {@link List} with found entities.
     */
    @Query("select t "
            + "from #{#entityName} t "
            + "where "
            + "trim(t.id) like lower(concat('%', :searchTerm, '%'))"
    )
    List<T> search(@Param("searchTerm") final String search);
}
