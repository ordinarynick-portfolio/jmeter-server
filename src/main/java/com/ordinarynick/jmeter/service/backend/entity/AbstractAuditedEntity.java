package com.ordinarynick.jmeter.service.backend.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.MappedSuperclass;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Common super class for all audited DB entities.
 */
@MappedSuperclass
@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PRIVATE)
public abstract class AbstractAuditedEntity extends AbstractEntity {

    /**
     * Date and time when was run created.
     */
    @CreatedDate
    OffsetDateTime created;

    /**
     * Date and time when was last change.
     */
    @LastModifiedDate
    OffsetDateTime lastChange;

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final AbstractAuditedEntity that = (AbstractAuditedEntity) o;
        return Objects.equals(getId(), that.getId())
                && Objects.equals(created, that.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), created);
    }
}
