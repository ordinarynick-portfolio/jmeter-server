package com.ordinarynick.jmeter.service.backend.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Represents one row of log run of {@link TestPlanRun}.
 */
@Entity
@Getter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class TestPlanRunLogRow extends AbstractEntity implements Serializable {

    /**
     * Date and time when row appeared.
     */
    @NotNull
    @Builder.Default
    OffsetDateTime date = OffsetDateTime.now();

    /**
     * Log row data. Usually one line of log.
     */
    String data;

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestPlanRunLogRow)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final TestPlanRunLogRow that = (TestPlanRunLogRow) o;
        return Objects.equals(getId(), that.getId())
                && Objects.equals(date, that.date)
                && Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), date, data);
    }
}
