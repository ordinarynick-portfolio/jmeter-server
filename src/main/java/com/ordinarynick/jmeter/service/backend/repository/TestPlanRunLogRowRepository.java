package com.ordinarynick.jmeter.service.backend.repository;

import com.ordinarynick.jmeter.service.backend.entity.TestPlanRunLogRow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Database {@link JpaRepository} for {@link TestPlanRunLogRow} entity.
 */
@Repository
public interface TestPlanRunLogRowRepository extends CommonJpaRepository<TestPlanRunLogRow> {

}
