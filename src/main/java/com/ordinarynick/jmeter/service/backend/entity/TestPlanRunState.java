package com.ordinarynick.jmeter.service.backend.entity;

/**
 * State of test plan.
 */
public enum TestPlanRunState {
    /**
     * Test plan is in pending, that means tha it is waiting in queue to be run.
     */
    PENDING,
    /**
     * Test plan is running.
     */
    RUNNING,
    /**
     * Test plan failed run.
     */
    FAILED,
    /**
     * Test plan has succeed.
     */
    SUCCESS,
    /**
     * Test plan has been canceled.
     */
    CANCELLED
}
