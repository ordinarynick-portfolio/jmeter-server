package com.ordinarynick.jmeter.service.backend.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents one JMeter test plan run.
 */
@Entity
@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@EntityListeners(AuditingEntityListener.class)
public class TestPlanRun extends AbstractAuditedEntity implements Serializable {

    /**
     * Log row for this run.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    TestPlan testPlan;

    /**
     * Url of service for which should be test plan run.
     */
    @URL
    @NotNull
    @NotEmpty
    String environmentUrl;

    /**
     * Test plan state, for example if it running.
     */
    @Enumerated(EnumType.STRING)
    TestPlanRunState state;

    /**
     * Log row for this run.
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    List<TestPlanRunLogRow> logEntries = new ArrayList<>();

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestPlanRun)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final TestPlanRun that = (TestPlanRun) o;
        return Objects.equals(getId(), that.getId())
                && Objects.equals(testPlan, that.testPlan)
                && Objects.equals(environmentUrl, that.environmentUrl)
                && state == that.state
                && Objects.equals(logEntries, that.logEntries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), testPlan, environmentUrl, state, logEntries);
    }
}
