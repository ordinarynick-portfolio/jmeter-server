package com.ordinarynick.jmeter.service.backend.repository;

import com.ordinarynick.jmeter.service.backend.entity.TestPlanRun;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Database {@link JpaRepository} for {@link TestPlanRun} entity.
 */
@Repository
public interface TestPlanRunRepository extends CommonJpaRepository<TestPlanRun> {

    @Query("select tpr "
            + "from TestPlanRun tpr "
            + "join fetch tpr.testPlan "
            + "where "
            + "trim(tpr.id) like lower(concat('%', :searchTerm, '%')) "
            + "or lower(tpr.environmentUrl) like lower(concat('%', :searchTerm, '%')) "
            + "or lower(tpr.state) like lower(concat('%', :searchTerm, '%')) "
            + "or lower(tpr.testPlan.name) like lower(concat('%', :searchTerm, '%'))"
            + "or lower(tpr.created) like lower(concat('%', :searchTerm, '%')) "
            + "or lower(tpr.lastChange) like lower(concat('%', :searchTerm, '%'))"
    )
    @Override
    List<TestPlanRun> search(@Param("searchTerm") final String search);
}
