package com.ordinarynick.jmeter.service.backend.service;

import com.ordinarynick.jmeter.service.backend.entity.TestPlan;
import com.ordinarynick.jmeter.service.backend.repository.TestPlanRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for {@link TestPlan} entity.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Slf4j
public class TestPlanService extends CrudService<TestPlan> {

    @Autowired
    public TestPlanService(final TestPlanRepository repository) {
        super(repository);
    }
}
