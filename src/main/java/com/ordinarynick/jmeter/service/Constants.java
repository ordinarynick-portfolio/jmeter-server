package com.ordinarynick.jmeter.service;

/**
 * Common constants for this application.
 */
public interface Constants {

    /**
     * Presentable name of application.
     */
    String APP_NAME = "JMeter Service";
}
