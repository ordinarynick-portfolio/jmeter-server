package com.ordinarynick.jmeter.service.security;

import org.springframework.security.web.savedrequest.HttpSessionRequestCache;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Custom request cache, which filters internal Vaadin requests.
 */
class CustomRequestCache extends HttpSessionRequestCache {

    @Override
    public void saveRequest(final HttpServletRequest request, final HttpServletResponse response) {
        if (!SecurityUtils.isFrameworkInternalRequest(request)) {
            super.saveRequest(request, response);
        }
    }

}
