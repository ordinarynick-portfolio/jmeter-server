package com.ordinarynick.jmeter.service.security;

import com.vaadin.flow.server.HandlerHelper.RequestType;
import com.vaadin.flow.shared.ApplicationConstants;
import lombok.experimental.UtilityClass;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

/**
 * Utility class for Spring security feature.
 */
@UtilityClass
public final class SecurityUtils {

    /**
     * If a {@link HttpServletRequest} is internal to Vaadin.
     *
     * @param request {@link HttpServletRequest} to check.
     * @return True if the {@link HttpServletRequest} is internal, otherwise false.
     */
    static boolean isFrameworkInternalRequest(final HttpServletRequest request) {
        return Optional.ofNullable(request.getParameter(ApplicationConstants.REQUEST_TYPE_PARAMETER))
                .stream()
                .anyMatch(parameterValue -> Arrays.stream(RequestType.values())
                        .anyMatch(r -> r.getIdentifier().equals(parameterValue)));
    }

    /**
     * Checks if user logged in or not.
     *
     * @return True if is logged, otherwise false.
     */
    static boolean isUserLoggedIn() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .filter(authentication -> !(authentication instanceof AnonymousAuthenticationToken))
                .map(Authentication::isAuthenticated)
                .orElse(false);
    }
}
