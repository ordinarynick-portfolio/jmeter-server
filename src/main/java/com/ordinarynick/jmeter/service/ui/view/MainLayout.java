package com.ordinarynick.jmeter.service.ui.view;

import com.ordinarynick.jmeter.service.ui.view.dashboard.DashboardView;
import com.ordinarynick.jmeter.service.ui.view.testplanrun.TestPlanRunsView;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.HighlightConditions;
import com.vaadin.flow.router.RouterLink;

@CssImport("./styles/shared-styles.css")
public class MainLayout extends AppLayout {

    public MainLayout() {
        createHeader();
        createDrawer();
    }

    private void createHeader() {
        final var logo = new H1("JMeter service");
        logo.addClassName("logo");

        final var logout = new Anchor("logout", "Log out");
        final var header = new HorizontalLayout(new DrawerToggle(), logo, logout);

        header.expand(logo);
        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        header.setWidth("100%");
        header.addClassName("header");

        addToNavbar(header);
    }

    private void createDrawer() {
        final var listLink = new RouterLink("Runs", TestPlanRunsView.class);
        listLink.setHighlightCondition(HighlightConditions.sameLocation());

        addToDrawer(new VerticalLayout(
                new RouterLink("Dashboard", DashboardView.class),
                listLink
        ));
    }
}
