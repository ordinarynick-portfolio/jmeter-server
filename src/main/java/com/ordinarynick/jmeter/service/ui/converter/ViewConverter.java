package com.ordinarynick.jmeter.service.ui.converter;

/**
 * View converter which converts backend entity to view entity and vice-versa.
 *
 * @param <E> Type of backend entity.
 * @param <V> Type of view entity.
 */
public interface ViewConverter<E, V> {

    /**
     * Converts entity to Dto.
     *
     * @param entity {@link E} which will be converted.
     * @return View dto {@link V} created from entity.
     */
    V toDto(final E entity);
}
