package com.ordinarynick.jmeter.service.ui.service;

import com.ordinarynick.jmeter.service.backend.service.TestPlanRunService;
import com.ordinarynick.jmeter.service.ui.converter.ViewTestPlanConverter;
import com.ordinarynick.jmeter.service.ui.converter.ViewTestPlanRunConverter;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRun;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * View service for {@link com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRun}.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Slf4j
public class ViewTestPlanRunService {

    TestPlanRunService testPlanRunService;

    /**
     * Finds all {@link ViewTestPlanRun} on backend.
     *
     * @param search {@link String} for full text search.
     * @return {@link List} with all found {@link ViewTestPlanRun}.
     */
    @Transactional(readOnly = true)
    public List<ViewTestPlanRun> findAll(final String search) {
        return testPlanRunService.findAll(search)
                .stream()
                .map(ViewTestPlanRunConverter::toDto)
                .collect(Collectors.toList());
    }

    /**
     * Run {@link ViewTestPlanRun}.
     *
     * @param viewTestPlanRun This {@link ViewTestPlanRun} will scheduled to run.
     */
    public void run(final ViewTestPlanRun viewTestPlanRun) {
        testPlanRunService.run(ViewTestPlanConverter.toEntity(viewTestPlanRun.getTestPlan()),
                viewTestPlanRun.getEnvironmentUrl());
    }
}
