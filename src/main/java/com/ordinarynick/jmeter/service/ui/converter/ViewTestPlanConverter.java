package com.ordinarynick.jmeter.service.ui.converter;

import com.ordinarynick.jmeter.service.backend.entity.TestPlan;
import com.ordinarynick.jmeter.service.backend.entity.TestPlanRun;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlan;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRun;
import lombok.experimental.UtilityClass;

import java.util.Optional;

/**
 * Convertor between backend {@link TestPlan} and view entity as {@link ViewTestPlan}.
 */
@UtilityClass
public class ViewTestPlanConverter {

    /**
     * Converts entity to Dto.
     *
     * @param entity {@link TestPlan} which will be converted.
     * @return View dto {@link ViewTestPlan} created from entity.
     */
    public ViewTestPlan toDto(final TestPlan entity) {
        return Optional.ofNullable(entity)
                .map(e -> (ViewTestPlan) new ViewTestPlan()
                        .setCreated(ViewCommonConverter.toString(e.getCreated()))
                        .setFile(e.getFile())
                        .setLastChange(ViewCommonConverter.toString(e.getLastChange()))
                        .setName(e.getName())
                        .setId(e.getId()))
                .orElse(null);
    }

    /**
     * Converts dto to entity.
     *
     * @param dto {@link ViewTestPlanRun} to convert.
     * @return Entity {@link TestPlanRun} converted from view dto.
     */
    public static TestPlan toEntity(final ViewTestPlan dto) {
        return Optional.ofNullable(dto)
                .map(d -> TestPlan.builder()
                        .created(ViewCommonConverter.toEntity(d.getCreated()))
                        .file(d.getFile())
                        .lastChange(ViewCommonConverter.toEntity(d.getLastChange()))
                        .name(d.getName())
                        .id(d.getId())
                        .build())
                .orElse(null);
    }
}
