package com.ordinarynick.jmeter.service.ui.view.testplanrun;

import com.ordinarynick.jmeter.service.backend.entity.TestPlanRunState;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlan;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRun;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRunLogRow;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Optional;

@Slf4j
@FieldDefaults(level = AccessLevel.PACKAGE, makeFinal = true)
public class TestPlanRunForm extends FormLayout {

    ComboBox<ViewTestPlan> cmbTestPlan = new ComboBox<>("Test plan");
    TextField txtEnvironmentUrl = new TextField("Environment URL");
    ComboBox<TestPlanRunState> cmbState = new ComboBox<>("State");
    TextField txtCreated = new TextField("Created");
    TextField txtLastChange = new TextField("Last change");
    Grid<ViewTestPlanRunLogRow> gridTestPlanRunLogRow = new Grid<>(ViewTestPlanRunLogRow.class);

    Button btnRun = new Button("Run");
    Button btnClose = new Button("Cancel");

    @NonFinal
    ViewTestPlanRun testPlanRun;
    Binder<ViewTestPlanRun> binder = new BeanValidationBinder<>(ViewTestPlanRun.class);

    public TestPlanRunForm(final Collection<ViewTestPlan> testPlans) {
        createUi();
        configureGrid();

        setData(testPlans);
        addListeners();
    }

    private void addListeners() {
        btnRun.addClickListener(event -> validateAndRun());
        btnClose.addClickListener(event -> fireEvent(new CloseEvent(this)));

        binder.addStatusChangeListener(e -> btnRun.setEnabled(binder.isValid()));
    }

    private void validateAndRun() {
        try {
            binder.writeBean(testPlanRun);
            fireEvent(new RunEvent(this, testPlanRun));
        } catch (final ValidationException exception) {
            Notification.show("Some error in form!");
        }
    }

    /**
     * Sets {@link ViewTestPlanRun} to this form. (Will be showed to user in this form.)
     *
     * @param testPlanRun {@link ViewTestPlanRun} to be shown.
     */
    public void setTestPlanRun(final ViewTestPlanRun testPlanRun) {
        this.testPlanRun = testPlanRun;
        binder.readBean(testPlanRun);
        Optional.ofNullable(testPlanRun)
                .map(ViewTestPlanRun::isPersisted)
                .filter(is -> is)
                .ifPresentOrElse(run -> setRunAgainView(true), () -> setRunAgainView(false));
    }

    private void setRunAgainView(final boolean runAgain) {
        btnRun.setText(runAgain ? "Run again" : "Run");

        cmbTestPlan.setReadOnly(runAgain);
        txtEnvironmentUrl.setReadOnly(runAgain);

        cmbState.setVisible(runAgain);
        txtCreated.setVisible(runAgain);
        txtLastChange.setVisible(runAgain);

        gridTestPlanRunLogRow.setVisible(runAgain);
    }

    private void setData(final Collection<ViewTestPlan> testPlans) {
        cmbTestPlan.setItems(testPlans);
        cmbTestPlan.setItemLabelGenerator(ViewTestPlan::getName);

        cmbState.setItems(TestPlanRunState.values());
    }

    private void createUi() {
        addClassName("test-plan-run-form");

        binder.forField(txtCreated).bind(ViewTestPlanRun::getCreated, null);
        binder.forField(txtLastChange).bind(ViewTestPlanRun::getLastChange, null);
        binder.forField(txtEnvironmentUrl).bind(ViewTestPlanRun::getEnvironmentUrl, ViewTestPlanRun::setEnvironmentUrl);

        binder.forField(cmbTestPlan).bind(ViewTestPlanRun::getTestPlan, ViewTestPlanRun::setTestPlan);
        binder.forField(cmbState).bind(viewTestPlanRun -> Optional.ofNullable(viewTestPlanRun.getState())
                .map(TestPlanRunState::valueOf).orElse(null), null);

        add(cmbTestPlan,
                txtEnvironmentUrl,
                cmbState,
                txtCreated,
                txtLastChange,
                gridTestPlanRunLogRow,
                createButtonsLayout());
    }

    private void configureGrid() {
        gridTestPlanRunLogRow.addClassNames("test-plan-run-log-grid");
        gridTestPlanRunLogRow.setColumns("date", "data");
        gridTestPlanRunLogRow.getColumns().forEach(column -> column.setAutoWidth(true));
        gridTestPlanRunLogRow.getElement().setAttribute("colspan", "2");
    }

    private HorizontalLayout createButtonsLayout() {
        btnRun.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        btnClose.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        btnRun.addClickShortcut(Key.ENTER);
        btnClose.addClickShortcut(Key.ESCAPE);

        return new HorizontalLayout(btnRun, btnClose);
    }

    /**
     * Common abstract event for all events on this form.
     */
    @Getter
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    public abstract static class TestPlanRunEvent extends ComponentEvent<TestPlanRunForm> {

        ViewTestPlanRun viewTestPlanRun;

        protected TestPlanRunEvent(final TestPlanRunForm source, final ViewTestPlanRun viewTestPlanRun) {
            super(source, false);

            this.viewTestPlanRun = viewTestPlanRun;
        }
    }

    /**
     * Run {@link ViewTestPlanRun} event.
     */
    public static class RunEvent extends TestPlanRunEvent {

        protected RunEvent(final TestPlanRunForm source,
                           final ViewTestPlanRun viewTestPlanRun) {
            super(source, viewTestPlanRun);
        }
    }

    /**
     * Close of this form event.
     */
    public static class CloseEvent extends TestPlanRunEvent {

        protected CloseEvent(final TestPlanRunForm source) {
            super(source, null);
        }
    }

    @Override
    public <T extends ComponentEvent<?>> Registration addListener(final Class<T> eventType,
                                                                  final ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
