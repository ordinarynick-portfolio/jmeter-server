package com.ordinarynick.jmeter.service.ui.service;

import com.ordinarynick.jmeter.service.backend.service.TestPlanService;
import com.ordinarynick.jmeter.service.ui.converter.ViewTestPlanConverter;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlan;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRun;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * View service for {@link ViewTestPlan}.
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Slf4j
public class ViewTestPlanService {

    TestPlanService testPlanService;

    /**
     * Finds all {@link ViewTestPlan} on backend.
     *
     * @param search {@link String} for full text search.
     * @return {@link List} with all found {@link ViewTestPlanRun}.
     */
    @Transactional(readOnly = true)
    public List<ViewTestPlan> findAll(final String search) {
        return testPlanService.findAll(search)
                .stream()
                .map(ViewTestPlanConverter::toDto)
                .collect(Collectors.toList());
    }
}
