package com.ordinarynick.jmeter.service.ui.view.testplanrun;

import com.ordinarynick.jmeter.service.Constants;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlan;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRun;
import com.ordinarynick.jmeter.service.ui.service.ViewTestPlanRunService;
import com.ordinarynick.jmeter.service.ui.service.ViewTestPlanService;
import com.ordinarynick.jmeter.service.ui.view.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.util.Optional;

@Route(value = "", layout = MainLayout.class)
@PageTitle("Runs | " + Constants.APP_NAME)
@FieldDefaults(level = AccessLevel.PACKAGE, makeFinal = true)
public class TestPlanRunsView extends VerticalLayout {

    ViewTestPlanRunService testPlanRunService;

    Grid<ViewTestPlanRun> gridTestPlanRun = new Grid<>(ViewTestPlanRun.class);
    TextField txtSearch = new TextField();
    Button btnRun = new Button("Run");

    TestPlanRunForm testPlanRunForm;

    public TestPlanRunsView(final ViewTestPlanRunService testPlanRunService,
                            final ViewTestPlanService testPlanService) {
        this.testPlanRunService = testPlanRunService;
        this.testPlanRunForm = new TestPlanRunForm(testPlanService.findAll(null));

        createUi();
        configureUi();
        configureListeners();

        updateList();

        closeDetail();
    }

    private void updateList() {
        gridTestPlanRun.setItems(testPlanRunService.findAll(txtSearch.getValue()));
    }

    private void showRun(final ViewTestPlanRun testPlanRun) {
        if (testPlanRun == null) {
            closeDetail();
        } else {
            testPlanRunForm.setTestPlanRun(testPlanRun);
            testPlanRunForm.setVisible(true);
            addClassName("showingDetail");
        }
    }

    private void configureListeners() {
        txtSearch.addValueChangeListener(e -> updateList());

        gridTestPlanRun.asSingleSelect().addValueChangeListener(event -> showRun(event.getValue()));

        testPlanRunForm.addListener(TestPlanRunForm.RunEvent.class, this::runTestPlanRun);
        testPlanRunForm.addListener(TestPlanRunForm.CloseEvent.class, event -> closeDetail());

        btnRun.addClickListener(click -> newTestPlanRun());
    }

    private void newTestPlanRun() {
        gridTestPlanRun.asSingleSelect().clear();
        showRun(new ViewTestPlanRun());
    }

    private void runTestPlanRun(final TestPlanRunForm.RunEvent event) {
        final var testPlanRun = event.getViewTestPlanRun();

        testPlanRunService.run(testPlanRun);
        updateList();
        closeDetail();

        Notification.show("Scheduled run of the test plan (" + testPlanRun.getTestPlan().getName() +
                ") against " + testPlanRun.getEnvironmentUrl() + " environment.");
    }

    private void closeDetail() {
        testPlanRunForm.setTestPlanRun(null);
        testPlanRunForm.setVisible(false);
        removeClassName("showingDetail");
    }

    private void configureUi() {
        addClassName("list-view");
        setSizeFull();

        configureFilter();
        configureGrid();
    }

    private void configureGrid() {
        gridTestPlanRun.addClassNames("test-plan-run-grid", "flex-2");
        gridTestPlanRun.setSizeFull();
        gridTestPlanRun.setColumns("id", "state");
        gridTestPlanRun.addColumn(testPlanRun -> Optional.ofNullable(testPlanRun.getTestPlan())
                .map(ViewTestPlan::getName)
                .orElse(null)
        ).setHeader("Test plan");
        gridTestPlanRun.addColumns("environmentUrl", "created", "lastChange");
        gridTestPlanRun.getColumns().forEach(column -> column.setAutoWidth(true));
    }

    private void configureFilter() {
        txtSearch.setPlaceholder("Search for test plan run...");
        txtSearch.setClearButtonVisible(true);
        txtSearch.setValueChangeMode(ValueChangeMode.LAZY);
        txtSearch.setWidthFull();
    }

    private void createUi() {
        final var content = new Div(gridTestPlanRun, testPlanRunForm);
        content.addClassName("content");
        content.setSizeFull();

        add(createToolbar(), content);
    }

    private Component createToolbar() {
        final var toolbar = new HorizontalLayout(txtSearch, btnRun);
        toolbar.addClassName("toolbar");
        toolbar.setWidthFull();

        return toolbar;
    }
}
