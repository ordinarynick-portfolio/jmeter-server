package com.ordinarynick.jmeter.service.ui.converter;


import com.ordinarynick.jmeter.service.utils.DateUtils;
import lombok.experimental.UtilityClass;

import java.time.OffsetDateTime;
import java.util.Optional;

/**
 * Convertor between backend common entities and view entity as {@link String}.
 */
@UtilityClass
public class ViewCommonConverter {

    /**
     * Converts {@link OffsetDateTime} to {@link String}.
     *
     * @param offsetDateTime {@link OffsetDateTime} to be converted.
     * @return {@link String} or {@literal null}.
     */
    public static String toString(final OffsetDateTime offsetDateTime) {
        return Optional.ofNullable(offsetDateTime)
                .map(DateUtils::toString)
                .orElse(null);
    }

    /**
     * Converts {@link String} to {@link OffsetDateTime}.
     *
     * @param string {@link String} to be converted (parsed).
     * @return {@link OffsetDateTime} or {@literal null}.
     */
    public static OffsetDateTime toEntity(final String string) {
        return Optional.ofNullable(string)
                .map(DateUtils::toEntity)
                .orElse(null);
    }
}
