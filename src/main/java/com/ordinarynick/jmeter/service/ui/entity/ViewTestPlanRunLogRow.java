package com.ordinarynick.jmeter.service.ui.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ViewTestPlanRunLogRow {

    /**
     * When log row appeared.
     */
    String date;

    /**
     * Content of log row.
     */
    String data;
}
