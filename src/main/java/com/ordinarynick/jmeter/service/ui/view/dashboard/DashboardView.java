package com.ordinarynick.jmeter.service.ui.view.dashboard;

import com.ordinarynick.jmeter.service.Constants;
import com.ordinarynick.jmeter.service.ui.view.MainLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@Route(value = "dashboard", layout = MainLayout.class)
@PageTitle("Dashboard | " + Constants.APP_NAME)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DashboardView extends VerticalLayout {

    public DashboardView() {
        createUi();
        configureUi();
    }

    private void configureUi() {
        addClassName("dashboard-view");
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
    }

    private void createUi() {
    }
}
