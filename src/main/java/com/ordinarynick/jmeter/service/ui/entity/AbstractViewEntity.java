package com.ordinarynick.jmeter.service.ui.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Abstract view entity as backend {@link com.ordinarynick.jmeter.service.backend.entity.AbstractEntity}.
 */
@Data
@Accessors(chain = true)
public class AbstractViewEntity {

    /**
     * Identifier of backend entity.
     */
    Long id;

    /**
     * Check if entity is persisted.
     *
     * @return True if entity is persisted. False otherwise.
     */
    public boolean isPersisted() {
        return id != null;
    }
}
