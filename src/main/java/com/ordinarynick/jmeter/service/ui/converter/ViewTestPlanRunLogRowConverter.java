package com.ordinarynick.jmeter.service.ui.converter;

import com.ordinarynick.jmeter.service.backend.entity.TestPlanRunLogRow;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRunLogRow;
import lombok.experimental.UtilityClass;

import java.util.Optional;

/**
 * Convertor between backend {@link TestPlanRunLogRow} and view entity as {@link ViewTestPlanRunLogRow}.
 */
@UtilityClass
public class ViewTestPlanRunLogRowConverter {

    /**
     * Converts entity to Dto.
     *
     * @param entity {@link TestPlanRunLogRow} which will be converted.
     * @return View dto {@link ViewTestPlanRunLogRow} created from entity.
     */
    public ViewTestPlanRunLogRow toDto(final TestPlanRunLogRow entity) {
        return Optional.ofNullable(entity)
                .map(e -> new ViewTestPlanRunLogRow()
                        .setDate(ViewCommonConverter.toString(e.getDate()))
                        .setData(e.getData()))
                .orElse(null);
    }
}
