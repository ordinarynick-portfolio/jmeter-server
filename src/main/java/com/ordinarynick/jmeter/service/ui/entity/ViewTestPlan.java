package com.ordinarynick.jmeter.service.ui.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ViewTestPlan extends AbstractViewEntity {

    /**
     * Name of the test plan.
     */
    String name;

    /**
     * Path to file of test plan.
     */
    String file;

    /**
     * When was test plan created.
     */
    String created;

    /**
     * Last change of test plan.
     */
    String lastChange;
}
