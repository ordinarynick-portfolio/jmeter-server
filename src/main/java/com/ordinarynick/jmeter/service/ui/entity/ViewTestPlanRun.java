package com.ordinarynick.jmeter.service.ui.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * View entity for backend {@link com.ordinarynick.jmeter.service.backend.entity.TestPlanRun} entity.
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ViewTestPlanRun extends AbstractViewEntity {

    /**
     * Test plan which was run.
     */
    ViewTestPlan testPlan;

    /**
     * On this environment url test plan will be or was run.
     */
    String environmentUrl;

    /**
     * State of run.
     */
    String state;

    /**
     * When was run created.
     */
    String created;

    /**
     * Last change of run.
     */
    String lastChange;
}
