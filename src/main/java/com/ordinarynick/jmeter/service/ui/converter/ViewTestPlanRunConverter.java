package com.ordinarynick.jmeter.service.ui.converter;

import com.ordinarynick.jmeter.service.backend.entity.TestPlanRun;
import com.ordinarynick.jmeter.service.backend.entity.TestPlanRunState;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRun;
import lombok.experimental.UtilityClass;

import java.util.Optional;

/**
 * Convertor between backend {@link TestPlanRun} and view entity as {@link ViewTestPlanRun}.
 */
@UtilityClass
public class ViewTestPlanRunConverter {

    /**
     * Converts entity to Dto.
     *
     * @param entity {@link TestPlanRun} which will be converted.
     * @return View dto {@link ViewTestPlanRun} created from entity.
     */
    public ViewTestPlanRun toDto(final TestPlanRun entity) {
        return Optional.ofNullable(entity)
                .map(e -> (ViewTestPlanRun) new ViewTestPlanRun()
                        .setCreated(ViewCommonConverter.toString(e.getCreated()))
                        .setEnvironmentUrl(e.getEnvironmentUrl())
                        .setLastChange(ViewCommonConverter.toString(e.getLastChange()))
                        .setState(toString(e.getState()))
                        .setTestPlan(ViewTestPlanConverter.toDto(e.getTestPlan()))
                        .setId(e.getId()))
                .orElse(null);
    }

    private String toString(final TestPlanRunState entity) {
        return Optional.ofNullable(entity)
                .map(Enum::name)
                .orElse(null);
    }
}
