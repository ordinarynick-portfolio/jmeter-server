package com.ordinarynick.jmeter.service.ui.view.login;

import com.ordinarynick.jmeter.service.Constants;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@Route("login")
@PageTitle("Login | " + Constants.APP_NAME)
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class LoginView extends VerticalLayout implements BeforeEnterObserver {

    LoginForm login = new LoginForm();

    public LoginView() {
        createUi();
        configureUi();
        configureListeners();
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        // inform the user about an authentication error
        if (beforeEnterEvent.getLocation()
                .getQueryParameters()
                .getParameters()
                .containsKey("error")) {
            login.setError(true);
        }
    }

    private void configureListeners() {
        login.setAction("login");
    }

    private void configureUi() {
        addClassName("login-view");

        setSizeFull();
        setAlignItems(Alignment.CENTER);
        setJustifyContentMode(JustifyContentMode.CENTER);
    }

    private void createUi() {
        add(new H1(Constants.APP_NAME), login);
    }
}
