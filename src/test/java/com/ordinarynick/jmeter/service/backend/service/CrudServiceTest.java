package com.ordinarynick.jmeter.service.backend.service;

import com.ordinarynick.jmeter.service.backend.entity.AbstractEntity;
import com.ordinarynick.jmeter.service.backend.repository.CommonJpaRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
class CrudServiceTest {

    static List<Data> allData = List.of(
            new Data(1L),
            new Data(2L),
            new Data(3L)
    );

    CommonJpaRepository<Data> repository = mock(CommonJpaRepository.class);
    CrudService<Data> service = new CrudService<Data>(repository) {
    };

    @BeforeEach
    void beforeTest() {
        reset(repository);
    }

    private static Stream<Arguments> findAllSource() {
        return Stream.of(
                Arguments.of(
                        null,
                        allData
                ),
                Arguments.of(
                        "",
                        allData
                ),
                Arguments.of(
                        "       ",
                        allData
                ),
                Arguments.of(
                        "1",
                        List.of(new Data(1L))
                ),
                Arguments.of(
                        "D",
                        List.of()
                )
        );
    }

    @ParameterizedTest(name = "#{index} Find all by string \"{0}\"")
    @MethodSource("findAllSource")
    void findAll(final String search, final List<Data> expected) {
        // Prepare
        final var searchIsBlank = StringUtils.isBlank(search);

        // Behaviour
        doReturn(allData).when(repository).findAll();
        if (!searchIsBlank) {
            doReturn(expected).when(repository).search(search);
        }

        // Test
        final var result = service.findAll(search);

        // Assert
        assertThat(result, containsInAnyOrder(expected.toArray()));
        verify(repository, times(searchIsBlank ? 1 : 0)).findAll();
        verify(repository, times(searchIsBlank ? 0 : 1)).search(search);
    }

    private static class Data extends AbstractEntity {

        public Data(final Long id) {
            super(id);
        }
    }
}
