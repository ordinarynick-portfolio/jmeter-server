package com.ordinarynick.jmeter.service.ui.view.testplanrun;

import com.ordinarynick.jmeter.service.backend.entity.TestPlanRunState;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlan;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRun;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

class TestPlanRunFormTest {

    @Test
    void formFieldsPopulated_newRun() {
        // Prepare
        final var testPlans = List.of(new ViewTestPlan()
                .setName("Test plan")
                .setFile("/file/test.jmx"));
        final var form = new TestPlanRunForm(testPlans);
        final var testPlanRun = new ViewTestPlanRun();

        // Test
        form.setTestPlanRun(testPlanRun);

        // Assert
        assertThat(form.txtCreated.getValue(), is(""));
        assertThat(form.txtLastChange.getValue(), is(""));
        assertThat(form.txtEnvironmentUrl.getValue(), is(""));
        assertThat(form.cmbState.getValue(), nullValue());
        assertThat(form.cmbTestPlan.getValue(), nullValue());

        assertThat(form.cmbTestPlan.isVisible(), is(true));
        assertThat(form.txtEnvironmentUrl.isVisible(), is(true));
        assertThat(form.txtCreated.isVisible(), is(false));
        assertThat(form.txtLastChange.isVisible(), is(false));
        assertThat(form.cmbState.isVisible(), is(false));

        assertThat(form.cmbTestPlan.isReadOnly(), is(false));
        assertThat(form.txtEnvironmentUrl.isReadOnly(), is(false));
    }


    @Test
    void formFieldsPopulated_run() {
        // Prepare
        final var testPlans = List.of(new ViewTestPlan()
                .setName("Test plan")
                .setFile("/file/test.jmx"));
        final var form = new TestPlanRunForm(testPlans);
        final var testPlanRun = (ViewTestPlanRun) new ViewTestPlanRun()
                .setCreated("01:02:03 04-05-2021")
                .setEnvironmentUrl("https://enviroment.com")
                .setLastChange("02:03:04 05-06-2021")
                .setState("RUNNING")
                .setTestPlan(testPlans.get(0))
                .setId(1L);

        // Test
        form.setTestPlanRun(testPlanRun);

        // Assert
        assertThat(form.txtCreated.getValue(), is("01:02:03 04-05-2021"));
        assertThat(form.txtLastChange.getValue(), is("02:03:04 05-06-2021"));
        assertThat(form.txtEnvironmentUrl.getValue(), is("https://enviroment.com"));
        assertThat(form.cmbState.getValue(), is(TestPlanRunState.RUNNING));
        assertThat(form.cmbTestPlan.getValue(), is(testPlans.get(0)));

        assertThat(form.cmbTestPlan.isVisible(), is(true));
        assertThat(form.txtEnvironmentUrl.isVisible(), is(true));
        assertThat(form.txtCreated.isVisible(), is(true));
        assertThat(form.txtLastChange.isVisible(), is(true));
        assertThat(form.cmbState.isVisible(), is(true));

        assertThat(form.cmbTestPlan.isReadOnly(), is(true));
        assertThat(form.txtEnvironmentUrl.isReadOnly(), is(true));
    }

    @Test
    void saveEventHasCorrectValues() {
        // Prepare
        final var testPlans = List.of(new ViewTestPlan()
                .setName("Test plan")
                .setFile("/file/test.jmx"));
        final var form = new TestPlanRunForm(testPlans);
        final var testPlanRun = new ViewTestPlanRun();

        final var runTestPlanRef = new AtomicReference<ViewTestPlanRun>(null);
        form.addListener(TestPlanRunForm.RunEvent.class, e -> {
            runTestPlanRef.set(e.getViewTestPlanRun());
        });

        form.setTestPlanRun(testPlanRun);
        form.txtEnvironmentUrl.setValue("https://environment.com");
        form.cmbTestPlan.setValue(testPlans.get(0));

        // Test
        form.btnRun.click();

        // Assert
        final var runEntity = runTestPlanRef.get();

        assertThat(runEntity.getCreated(), nullValue());
        assertThat(runEntity.getLastChange(), nullValue());
        assertThat(runEntity.getEnvironmentUrl(), is("https://environment.com"));
        assertThat(runEntity.getState(), nullValue());
        assertThat(runEntity.getTestPlan(), is(testPlans.get(0)));
    }
}
