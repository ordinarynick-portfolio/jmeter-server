package com.ordinarynick.jmeter.service.ui.converter;

import com.ordinarynick.jmeter.service.backend.entity.TestPlanRunLogRow;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRunLogRow;
import com.ordinarynick.jmeter.service.utils.DateUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.samePropertyValuesAs;

class ViewTestPlanRunLogRowConverterTest {

    private static Stream<Arguments> toDtoSource() {
        final var date = OffsetDateTime.of(2021, 02, 03, 04, 05, 06, 07, ZoneOffset.UTC);
        final var dateString = DateUtils.toString(date);

        return Stream.of(
                Arguments.of(
                        "Null input",
                        null,
                        null
                ),
                Arguments.of(
                        "Empty object",
                        TestPlanRunLogRow.builder().build(),
                        new ViewTestPlanRunLogRow().setDate(ViewCommonConverter.toString(OffsetDateTime.now()))
                ),
                Arguments.of(
                        "Not saved TestPlanRunLogRow in DB (without id)",
                        TestPlanRunLogRow.builder()
                                .id(null)
                                .date(date)
                                .data("Test plan run log row data.")
                                .build(),
                        new ViewTestPlanRunLogRow()
                                .setDate(dateString)
                                .setData("Test plan run log row data.")
                ),
                Arguments.of(
                        "Fully filled TestPlanRunLogRow",
                        TestPlanRunLogRow.builder()
                                .id(3L)
                                .date(date)
                                .data("Test plan run log row data.")
                                .build(),
                        new ViewTestPlanRunLogRow()
                                .setDate(dateString)
                                .setData("Test plan run log row data.")
                )
        );
    }

    @ParameterizedTest(name = "#{index} {0}")
    @MethodSource("toDtoSource")
    void toDto(final String useCase, final TestPlanRunLogRow logRow, final ViewTestPlanRunLogRow expected) {
        // Test
        final var result = ViewTestPlanRunLogRowConverter.toDto(logRow);

        // Assert
        if (expected == null) {
            assertThat(result, nullValue());
        } else {
            assertThat(result, samePropertyValuesAs(expected));
        }
    }
}
