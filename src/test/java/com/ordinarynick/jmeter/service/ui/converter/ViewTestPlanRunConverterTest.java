package com.ordinarynick.jmeter.service.ui.converter;

import com.ordinarynick.jmeter.service.backend.entity.TestPlan;
import com.ordinarynick.jmeter.service.backend.entity.TestPlanRun;
import com.ordinarynick.jmeter.service.backend.entity.TestPlanRunState;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlan;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRun;
import com.ordinarynick.jmeter.service.utils.DateUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.samePropertyValuesAs;

class ViewTestPlanRunConverterTest {

    private static Stream<Arguments> toDtoSource() {
        final var date = OffsetDateTime.of(2021, 2, 3, 4, 5, 6, 7, ZoneOffset.UTC);
        final var dateString = DateUtils.toString(date);

        return Stream.of(
                Arguments.of(
                        "Null input",
                        null,
                        null
                ),
                Arguments.of(
                        "Empty object",
                        TestPlanRun.builder().build(),
                        new ViewTestPlanRun()
                ),
                Arguments.of(
                        "Not saved TestPlanRun in DB (without id, created, last change)",
                        TestPlanRun.builder()
                                .created(null)
                                .environmentUrl("https://environment.com")
                                .id(null)
                                .lastChange(null)
                                .state(TestPlanRunState.RUNNING)
                                .testPlan(null)
                                .build(),
                        new ViewTestPlanRun()
                                .setCreated(null)
                                .setEnvironmentUrl("https://environment.com")
                                .setLastChange(null)
                                .setState("RUNNING")
                                .setTestPlan(null)
                                .setId(null)
                ),
                Arguments.of(
                        "TestPlanRun without TestPlan entity",
                        TestPlanRun.builder()
                                .created(date)
                                .environmentUrl("https://environment.com")
                                .id(2L)
                                .lastChange(date)
                                .state(TestPlanRunState.RUNNING)
                                .testPlan(null)
                                .build(),
                        new ViewTestPlanRun()
                                .setCreated(dateString)
                                .setEnvironmentUrl("https://environment.com")
                                .setLastChange(dateString)
                                .setState("RUNNING")
                                .setTestPlan(null)
                                .setId(2L)
                ),
                Arguments.of(
                        "Fully filled TestPlanRun",
                        TestPlanRun.builder()
                                .created(date)
                                .environmentUrl("https://environment.com")
                                .id(2L)
                                .lastChange(date)
                                .state(TestPlanRunState.RUNNING)
                                .testPlan(TestPlan.builder()
                                        .file("/some/file.jmx")
                                        .id(1L)
                                        .name("TestPlan name")
                                        .build())
                                .build(),
                        new ViewTestPlanRun()
                                .setCreated(dateString)
                                .setEnvironmentUrl("https://environment.com")
                                .setLastChange(dateString)
                                .setState("RUNNING")
                                .setTestPlan((ViewTestPlan) new ViewTestPlan()
                                        .setFile("/some/file.jmx")
                                        .setName("TestPlan name")
                                        .setId(1L))
                                .setId(2L)
                )
        );
    }

    @ParameterizedTest(name = "#{index} {0}")
    @MethodSource("toDtoSource")
    void toDto(final String useCase, final TestPlanRun testPlanRun, final ViewTestPlanRun expected) {
        // Test
        final var result = ViewTestPlanRunConverter.toDto(testPlanRun);

        // Assert
        if (expected == null) {
            assertThat(result, nullValue());
        } else {
            assertThat(result, samePropertyValuesAs(expected));
        }
    }
}
