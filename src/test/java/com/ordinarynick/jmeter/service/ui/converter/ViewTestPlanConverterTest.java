package com.ordinarynick.jmeter.service.ui.converter;

import com.ordinarynick.jmeter.service.backend.entity.TestPlan;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlan;
import com.ordinarynick.jmeter.service.utils.DateUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.samePropertyValuesAs;

class ViewTestPlanConverterTest {

    private static Stream<Arguments> toDtoSource() {
        final var date = OffsetDateTime.of(2021, 2, 3, 4, 5, 6, 7, ZoneOffset.UTC);
        final var dateString = DateUtils.toString(date);

        return Stream.of(
                Arguments.of(
                        "Null input",
                        null,
                        null
                ),
                Arguments.of(
                        "Empty object",
                        TestPlan.builder().build(),
                        new ViewTestPlan()
                ),
                Arguments.of(
                        "Not saved TestPlan in DB (without id, created, last change)",
                        TestPlan.builder()
                                .created(null)
                                .file("/some/file.jmx")
                                .id(null)
                                .lastChange(null)
                                .name("Test plan name")
                                .build(),
                        new ViewTestPlan()
                                .setCreated(null)
                                .setFile("/some/file.jmx")
                                .setLastChange(null)
                                .setName("Test plan name")
                                .setId(null)
                ),
                Arguments.of(
                        "Fully filled TestPlan",
                        TestPlan.builder()
                                .created(date)
                                .file("/some/file.jmx")
                                .id(2L)
                                .lastChange(date)
                                .name("Test plan name")
                                .build(),
                        new ViewTestPlan()
                                .setCreated(dateString)
                                .setFile("/some/file.jmx")
                                .setLastChange(dateString)
                                .setName("Test plan name")
                                .setId(2L)
                )
        );
    }

    @ParameterizedTest(name = "#{index} {0}")
    @MethodSource("toDtoSource")
    void toDto(final String useCase, final TestPlan testPlan, final ViewTestPlan expected) {
        // Test
        final var result = ViewTestPlanConverter.toDto(testPlan);

        // Assert
        if (expected == null) {
            assertThat(result, nullValue());
        } else {
            assertThat(result, samePropertyValuesAs(expected));
        }
    }

    private static Stream<Arguments> toEntitySource() {
        final var systemOffset = OffsetDateTime.now().getOffset();
        final var date = OffsetDateTime.of(2021, 2, 3, 4, 5, 6, 0, systemOffset);
        final var dateString = DateUtils.toString(date);

        return Stream.of(
                Arguments.of(
                        "Null input",
                        null,
                        null
                ),
                Arguments.of(
                        "Empty object",
                        new ViewTestPlan(),
                        TestPlan.builder().build()
                ),
                Arguments.of(
                        "Not saved TestPlan in DB (without id, created, last change)",
                        new ViewTestPlan()
                                .setCreated(null)
                                .setFile("/some/file.jmx")
                                .setLastChange(null)
                                .setName("Test plan name")
                                .setId(null),
                        TestPlan.builder()
                                .created(null)
                                .file("/some/file.jmx")
                                .id(null)
                                .lastChange(null)
                                .name("Test plan name")
                                .build()
                ),
                Arguments.of(
                        "Fully filled TestPlan",
                        new ViewTestPlan()
                                .setCreated(dateString)
                                .setFile("/some/file.jmx")
                                .setLastChange(dateString)
                                .setName("Test plan name")
                                .setId(2L),
                        TestPlan.builder()
                                .created(date)
                                .file("/some/file.jmx")
                                .id(2L)
                                .lastChange(date)
                                .name("Test plan name")
                                .build()
                )
        );
    }

    @ParameterizedTest(name = "#{index} {0}")
    @MethodSource("toEntitySource")
    void toEntity(final String usecase, final ViewTestPlan testPlan, final TestPlan expected) {
        // Test
        final var result = ViewTestPlanConverter.toEntity(testPlan);

        // Assert
        if (expected == null) {
            assertThat(result, nullValue());
        } else {
            assertThat(result, samePropertyValuesAs(expected));
        }
    }
}
