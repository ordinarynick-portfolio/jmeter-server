package com.ordinarynick.jmeter.service.ui.view.testplanrun;

import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlan;
import com.ordinarynick.jmeter.service.ui.entity.ViewTestPlanRun;
import com.ordinarynick.jmeter.service.ui.service.ViewTestPlanRunService;
import com.ordinarynick.jmeter.service.ui.service.ViewTestPlanService;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.provider.ListDataProvider;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
class TestPlanRunsViewTest {

    ViewTestPlanService testPlanService = mock(ViewTestPlanService.class);
    ViewTestPlanRunService testPlanRunService = mock(ViewTestPlanRunService.class);
    List<ViewTestPlan> testPlanList = List.of(
            (ViewTestPlan) new ViewTestPlan().setName("Test Plan 0").setFile("somefile.jmx").setId(1L)
    );
    List<ViewTestPlanRun> testPlanRuns = List.of(
            (ViewTestPlanRun) new ViewTestPlanRun().setTestPlan(testPlanList.get(0))
                    .setEnvironmentUrl("someurl.com")
                    .setId(1L),
            (ViewTestPlanRun) new ViewTestPlanRun().setTestPlan(testPlanList.get(0))
                    .setEnvironmentUrl("someurl.net")
                    .setId(2L)
    );


    @BeforeEach
    void beforeTest() {
        reset(testPlanRunService, testPlanService);

        doReturn(testPlanList).when(testPlanService).findAll(any());
        doReturn(testPlanRuns).when(testPlanRunService).findAll(any());
        doReturn(List.of(testPlanRuns.get(1))).when(testPlanRunService).findAll("2");
    }

    @Test
    void gridShowAllRuns() {
        // Prepare
        final var view = new TestPlanRunsView(testPlanRunService, testPlanService);

        // Test
        final var result = getGridItems(view.gridTestPlanRun);

        // Assert
        assertThat(result, containsInAnyOrder(testPlanRuns.toArray()));
    }

    @Test
    void viewSearchWorks() {
        // Prepare
        final var view = new TestPlanRunsView(testPlanRunService, testPlanService);

        // Test
        view.txtSearch.setValue("2");
        final var result = getGridItems(view.gridTestPlanRun);

        // Assert
        assertThat(result, hasSize(1));
        assertThat(result, hasItem(testPlanRuns.get(1)));

        verify(testPlanRunService, times(1)).findAll("2");
    }

    @Test
    void formShownWhenContactSelected() {
        // Prepare
        final var view = new TestPlanRunsView(testPlanRunService, testPlanService);
        final var firstItemInGrid = getGridItems(view.gridTestPlanRun).iterator().next();

        // Preconditions
        assertThat(view.testPlanRunForm.isVisible(), is(false));

        // Test
        view.gridTestPlanRun.asSingleSelect().setValue(firstItemInGrid);

        // Assert
        assertThat(view.testPlanRunForm.isVisible(), is(true));
        assertThat(view.testPlanRunForm.txtEnvironmentUrl.getValue(), is(firstItemInGrid.getEnvironmentUrl()));
    }

    private Collection<ViewTestPlanRun> getGridItems(final Grid<ViewTestPlanRun> grid) {
        return ((ListDataProvider<ViewTestPlanRun>) grid.getDataProvider()).getItems();
    }
}
