package com.ordinarynick.jmeter.service.ui.converter;

import com.ordinarynick.jmeter.service.utils.DateUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class ViewCommonConverterTest {

    private static Stream<Arguments> testToStringSource() {
        final var date = OffsetDateTime.of(2021, 2, 3, 4, 5, 6, 7, ZoneOffset.UTC);

        return Stream.of(
                Arguments.of(
                        "Null input",
                        null,
                        null
                ),
                Arguments.of(
                        "Some offset date time.",
                        date,
                        DateUtils.toString(date)
                )
        );
    }

    @ParameterizedTest(name = "#{index} {0}")
    @MethodSource("testToStringSource")
    void testToString(final String useCase, final OffsetDateTime offsetDateTime, final String expected) {
        assertThat(ViewCommonConverter.toString(offsetDateTime), is(expected));
    }

    private static Stream<Arguments> toEntitySource() {
        final var systemOffset = OffsetDateTime.now().getOffset();
        final var date = OffsetDateTime.of(2021, 2, 3, 4, 5, 6, 0, systemOffset);

        return Stream.of(
                Arguments.of(
                        "Null input",
                        null,
                        null
                ),
                Arguments.of(
                        "Some offset date time.",
                        DateUtils.toString(date),
                        date
                )
        );
    }

    @ParameterizedTest(name = "#{index} {0}")
    @MethodSource("toEntitySource")
    void toEntity(final String useCase, final String date, final OffsetDateTime expected) {
        assertThat(ViewCommonConverter.toEntity(date), is(expected));
    }
}
