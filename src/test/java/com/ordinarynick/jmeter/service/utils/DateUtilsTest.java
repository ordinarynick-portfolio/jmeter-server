package com.ordinarynick.jmeter.service.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DateUtilsTest {

    private static Stream<Arguments> testToStringSource() {
        final var systemOffset = OffsetDateTime.now().getOffset();
        final var utcAndSystemOffsetDifference = systemOffset.getTotalSeconds() - ZoneOffset.UTC.getTotalSeconds();
        final var offsetUtcMidnight = LocalDateTime.of(2021, 1, 1, 0, 0, 0)
                .plusSeconds(utcAndSystemOffsetDifference);

        return Stream.of(
                Arguments.of(
                        "Normal date.",
                        OffsetDateTime.of(2021, 1, 2, 3, 4, 5, 6, systemOffset),
                        "03:04:05 02-01-2021"
                ),
                Arguments.of(
                        "Before last midnight of the year.",
                        OffsetDateTime.of(2021, 12, 31, 23, 59, 59, 999, systemOffset),
                        "23:59:59 31-12-2021"
                ),
                Arguments.of(
                        "After last midnight of the previous year.",
                        OffsetDateTime.of(2021, 1, 1, 0, 0, 0, 0, systemOffset),
                        "00:00:00 01-01-2021"
                ),
                Arguments.of(
                        "After last midnight of the previous year with System Zone offset.",
                        OffsetDateTime.of(2021, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC),
                        offsetUtcMidnight.format(DateTimeFormatter.ofPattern("hh:mm:ss dd-MM-yyyy"))
                )
        );
    }

    @ParameterizedTest(name = "#{index} {0} ({2})")
    @MethodSource("testToStringSource")
    void testToString(final String caseName, final OffsetDateTime date, final String expected) {
        assertEquals(expected, DateUtils.toString(date));
    }

    private static Stream<Arguments> toEntitySource() {
        final var systemOffset = OffsetDateTime.now().getOffset();
        final var utcAndSystemOffsetDifference = systemOffset.getTotalSeconds() - ZoneOffset.UTC.getTotalSeconds();
        final var offsetUtcMidnight = LocalDateTime.of(2021, 1, 1, 0, 0, 0)
                .plusSeconds(utcAndSystemOffsetDifference);

        return Stream.of(
                Arguments.of(
                        "Normal date.",
                        "03:04:05 02-01-2021",
                        OffsetDateTime.of(2021, 1, 2, 3, 4, 5, 0, systemOffset)
                ),
                Arguments.of(
                        "Before last midnight of the year.",
                        "23:59:59 31-12-2021",
                        OffsetDateTime.of(2021, 12, 31, 23, 59, 59, 0, systemOffset)
                ),
                Arguments.of(
                        "After last midnight of the previous year.",
                        "00:00:00 01-01-2021",
                        OffsetDateTime.of(2021, 1, 1, 0, 0, 0, 0, systemOffset)
                )
        );
    }

    @ParameterizedTest(name = "#{index} {0} ({2})")
    @MethodSource("toEntitySource")
    void toEntity(final String caseName, final String date, final OffsetDateTime expected) {
        assertEquals(expected, DateUtils.toEntity(date));
    }
}
